"""
Module: _helpers.py
Author: Pedro Garcia Freitas
Date: Oct 22, 2024
Description: Module to compute the histogram of LBP labels.
"""

from collections import Counter
from typing import Callable

import numpy as np
import open3d as o3d
from open3d.cpu.pybind import geometry


def compute_feature_histogram(
    pcd: o3d.geometry.PointCloud, number_bins
) -> o3d.geometry.PointCloud:
    if pcd.has_colors() and len(pcd.colors) == len(pcd.points):
        colors = np.asarray(pcd.colors, dtype=np.float64)
        if np.all((colors >= 0) & (colors <= 1)) and (number_bins > 2):
            colors *= 255.0
        colors = np.asarray(colors, dtype=np.uint8)
        histogram = [0 for _ in range(int(number_bins))]
        counters = Counter(colors[:, 0])
        for k, v in counters.items():
            try:
                histogram[k - 1] = v
            except Exception:
                import sys

                function_name = sys._getframe().f_code.co_name
                print(f" Error in {function_name} -> k={k}, v={v}")
                raise
        return np.array(histogram)
    else:
        raise ValueError("Point cloud does not have color attributes")


def abstract_lbp_iterator_feature_extract(
    pcd: o3d.geometry.PointCloud, neighbors: int, iterator: Callable
) -> o3d.geometry.PointCloud:
    return abstract_iterator_applicator(
        pcd, neighbors, iterator, apply_iterator
    )


def abstract_iterator_applicator(
    pcd: o3d.geometry.PointCloud,
    neighbors: int,
    iterator: Callable,
    applicator: Callable,
) -> o3d.geometry.PointCloud:
    if pcd.has_colors() and len(pcd.colors) == len(pcd.points):
        return applicator(iterator, neighbors, pcd)
    else:
        raise ValueError("Point cloud does not have color attributes")


def apply_iterator(iterator, neighbors, pcd):
    geometry, gray_colors = get_geometry_and_grayscale_from_pc(pcd)
    map_labels = iterator(geometry, gray_colors, neighbors)
    feature_map = labels_to_feature_map(geometry, map_labels)
    return feature_map


def labels_to_feature_map(geometry, map_labels):
    feature_map = o3d.geometry.PointCloud()
    feature_map.points = o3d.utility.Vector3dVector(geometry)
    grayscale_colors_rgb = np.tile(map_labels, (1, 3))
    grayscale = grayscale_colors_rgb.astype(np.float64) / 255.0
    feature_map.colors = o3d.utility.Vector3dVector(grayscale)
    return feature_map


def get_geometry_and_grayscale_from_pc(pcd):
    geometry = np.asarray(pcd.points, dtype=np.float64)
    colors = np.asarray(pcd.colors, dtype=np.float64)
    # BT.709 luminance formula
    gray_colors = np.dot(colors[:, :3], [0.2126, 0.7152, 0.0722])
    return geometry, gray_colors


def abstract_complete_lbp_iterator_feature_extract(
    pcd: geometry.PointCloud, neighbors: int, iterator: Callable
) -> geometry.PointCloud:
    return abstract_iterator_applicator(
        pcd, neighbors, iterator, apply_complete_lbp_iterator
    )


def apply_complete_lbp_iterator(iterator, neighbors, pcd):
    geometry, gray_colors = get_geometry_and_grayscale_from_pc(pcd)
    mean_value = sum(gray_colors) / len(gray_colors)
    gray_colors_size = len(gray_colors)
    map_labels = iterator(
        geometry,
        gray_colors,
        neighbors,
        mean_value,
        gray_colors_size,
    )
    feature_map = labels_to_feature_map(geometry, map_labels)
    return feature_map
