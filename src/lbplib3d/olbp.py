"""
Module: olbp.py
Author: Pedro Garcia Freitas
Date: May 17, 2024
Description: This module provides functions for computing the original
local binary pattern (LBP) feature maps.
"""

import numpy as np
import numpy.typing as npt
import open3d as o3d
from lbplib3d.extensions.olbp_module import olbp_module
from lbplib3d._helpers import compute_feature_histogram
from lbplib3d._helpers import abstract_lbp_iterator_feature_extract


def olbp(
    pcd: o3d.geometry.PointCloud, neighbors: int = 8
) -> o3d.geometry.PointCloud:
    """
    Computes the Original Local Binary Pattern (OLBP) feature map of an
    input color Point Cloud.

    Parameters:
        pcd (o3d.geometry.PointCloud): Input point cloud from open3d.
        neighbors (int): Number of LBP neighbors

    Returns:
        o3d.geometry.PointCloud: 3D feature map computed using OLBP algorithm.
    """
    return abstract_lbp_iterator_feature_extract(
        pcd, neighbors, olbp_module.iterator_olbp
    )


def olbp_feature(
    pcd: o3d.geometry.PointCloud, neighbors: int = 8
) -> npt.NDArray[np.int_]:
    """
    Computes the histogram of the Original Local Binary Pattern (OLBP) from
    the feature map of an input color Point Cloud.

    Parameters:
        pcd (o3d.geometry.PointCloud): Input point cloud from open3d.
        neighbors (int): Number of LBP neighbors

    Returns:
        o3d.geometry.PointCloud: Histogram features of OLBP feature map.
    """
    feature_map = olbp(pcd, neighbors)
    return compute_feature_histogram(feature_map, number_bins=2**neighbors)
