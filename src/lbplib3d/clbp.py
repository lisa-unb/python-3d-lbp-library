"""
Module: clbp.py
Author: Pedro Garcia Freitas
Date: May 17, 2024
Description: This module provides functions for computing the Complete
Local Binary Pattern (CLBP) feature maps.
"""

import numpy as np
import numpy.typing as npt
from open3d import geometry
from typing import Tuple
from lbplib3d.extensions.clbp_center_module import clbp_center_module
from lbplib3d.extensions.clbp_magnitude_module import clbp_magnitude_module
from lbplib3d.extensions.clbp_signs_module import clbp_signs_module
from open3d.cpu.pybind.geometry import PointCloud

from lbplib3d._helpers import (
    compute_feature_histogram,
    abstract_complete_lbp_iterator_feature_extract,
)


def clbp_magnitude(
    pcd: geometry.PointCloud, neighbors: int = 8
) -> geometry.PointCloud:
    """
    Computes the Original Local Binary Pattern (OLBP) feature map of an
    input color Point Cloud.

    Parameters:
        pcd (o3d.geometry.PointCloud): Input point cloud from open3d.
        neighbors (int): Number of LBP neighbors

    Returns:
        o3d.geometry.PointCloud: 3D feature map computed using OLBP algorithm.
    """
    return abstract_complete_lbp_iterator_feature_extract(
        pcd, neighbors, clbp_magnitude_module.iterator_clbp_magnitude
    )


def clbp_signs(
    pcd: geometry.PointCloud, neighbors: int = 8
) -> geometry.PointCloud:
    """
    Computes the Original Local Binary Pattern (OLBP) feature map of an
    input color Point Cloud.

    Parameters:
        pcd (o3d.geometry.PointCloud): Input point cloud from open3d.
        neighbors (int): Number of LBP neighbors

    Returns:
        o3d.geometry.PointCloud: 3D feature map computed using OLBP algorithm.
    """
    return abstract_complete_lbp_iterator_feature_extract(
        pcd, neighbors, clbp_signs_module.iterator_clbp_signs
    )


def clbp_center(
    pcd: geometry.PointCloud, neighbors: int = 8
) -> geometry.PointCloud:
    """
    Computes the Center part of Complete Local Binary Pattern (CLBP) feature
    map of an input color Point Cloud.

    Parameters:
        pcd (o3d.geometry.PointCloud): Input point cloud from open3d.
        neighbors (int): Number of LBP neighbors

    Returns:
        o3d.geometry.PointCloud: 3D feature map computed using CLBP algorithm.
    """
    return abstract_complete_lbp_iterator_feature_extract(
        pcd, neighbors, clbp_center_module.iterator_clbp_center
    )


def clbp(
    pcd: geometry.PointCloud, neighbors: int = 8
) -> Tuple[PointCloud, PointCloud, PointCloud]:
    return (
        clbp_center(pcd, neighbors),
        clbp_magnitude(pcd, neighbors),
        clbp_signs(pcd, neighbors),
    )


def clbp_feature(
    pcd: geometry.PointCloud, neighbors: int = 8
) -> npt.NDArray[np.int_]:
    """
    Computes the histogram of the Complete Local Binary Pattern (CLBP) from
    the feature map of an input color Point Cloud.

    Parameters:
        pcd (o3d.geometry.PointCloud): Input point cloud from open3d.
        neighbors (int): Number of LBP neighbors

    Returns:
        o3d.geometry.PointCloud: Histogram features of CLBP feature map.
    """
    center, magnitude, signs = clbp(pcd, neighbors)
    histogram_center = compute_feature_histogram(center, number_bins=2)
    histogram_magnitude = compute_feature_histogram(
        magnitude, number_bins=2**neighbors
    )
    histogram_signs = compute_feature_histogram(
        signs, number_bins=2**neighbors
    )
    return np.concatenate(
        (histogram_center, histogram_magnitude, histogram_signs)
    )
