"""
Module: olbp.py
Author: Pedro Garcia Freitas
Date: May 17, 2024
Description: This module provides functions for computing the complete
local ternary pattern (CLTP) feature maps.
"""

import numpy as np
import numpy.typing as npt
from open3d import geometry
from lbplib3d.extensions.upper_cltp_magnitude_module import (
    upper_cltp_magnitude_module,
)
from lbplib3d.extensions.upper_cltp_signs_module import upper_cltp_signs_module
from lbplib3d.extensions.upper_cltp_center_module import (
    upper_cltp_center_module,
)
from lbplib3d.extensions.lower_cltp_magnitude_module import (
    lower_cltp_magnitude_module,
)
from lbplib3d.extensions.lower_cltp_signs_module import lower_cltp_signs_module
from lbplib3d.extensions.lower_cltp_center_module import (
    lower_cltp_center_module,
)

from lbplib3d._helpers import (
    compute_feature_histogram,
    abstract_complete_lbp_iterator_feature_extract,
)


def upper_cltp_magnitude(
    pcd: geometry.PointCloud, neighbors: int = 8
) -> geometry.PointCloud:
    """
    Computes the Original Local Binary Pattern (OLBP) feature map of an
    input color Point Cloud.

    Parameters:
        pcd (geometry.PointCloud): Input point cloud from open3d.
        neighbors (int): Number of LBP neighbors

    Returns:
        geometry.PointCloud: 3D feature map computed using OLBP algorithm.
    """
    return abstract_complete_lbp_iterator_feature_extract(
        pcd,
        neighbors,
        upper_cltp_magnitude_module.iterator_upper_cltp_magnitude,
    )


def upper_cltp_signs(
    pcd: geometry.PointCloud, neighbors: int = 8
) -> geometry.PointCloud:
    """
    Computes the Original Local Binary Pattern (OLBP) feature map of an
    input color Point Cloud.

    Parameters:
        pcd (geometry.PointCloud): Input point cloud from open3d.
        neighbors (int): Number of LBP neighbors

    Returns:
        geometry.PointCloud: 3D feature map computed using OLBP algorithm.
    """
    return abstract_complete_lbp_iterator_feature_extract(
        pcd, neighbors, upper_cltp_signs_module.iterator_upper_cltp_signs
    )


def upper_cltp_center(
    pcd: geometry.PointCloud, neighbors: int = 8
) -> geometry.PointCloud:
    """
    Computes the Original Local Binary Pattern (OLBP) feature map of an
    input color Point Cloud.

    Parameters:
        pcd (geometry.PointCloud): Input point cloud from open3d.
        neighbors (int): Number of LBP neighbors

    Returns:
        geometry.PointCloud: 3D feature map computed using OLBP algorithm.
    """
    return abstract_complete_lbp_iterator_feature_extract(
        pcd, neighbors, upper_cltp_center_module.iterator_upper_cltp_center
    )


def lower_cltp_magnitude(
    pcd: geometry.PointCloud, neighbors: int = 8
) -> geometry.PointCloud:
    """
    Computes the Original Local Binary Pattern (OLBP) feature map of an
    input color Point Cloud.

    Parameters:
        pcd (geometry.PointCloud): Input point cloud from open3d.
        neighbors (int): Number of LBP neighbors

    Returns:
        geometry.PointCloud: 3D feature map computed using OLBP algorithm.
    """
    return abstract_complete_lbp_iterator_feature_extract(
        pcd,
        neighbors,
        lower_cltp_magnitude_module.iterator_lower_cltp_magnitude,
    )


def lower_cltp_signs(
    pcd: geometry.PointCloud, neighbors: int = 8
) -> geometry.PointCloud:
    """
    Computes the Original Local Binary Pattern (OLBP) feature map of an
    input color Point Cloud.

    Parameters:
        pcd (geometry.PointCloud): Input point cloud from open3d.
        neighbors (int): Number of LBP neighbors

    Returns:
        geometry.PointCloud: 3D feature map computed using OLBP algorithm.
    """
    return abstract_complete_lbp_iterator_feature_extract(
        pcd, neighbors, lower_cltp_signs_module.iterator_lower_cltp_signs
    )


def lower_cltp_center(
    pcd: geometry.PointCloud, neighbors: int = 8
) -> geometry.PointCloud:
    """
    Computes the Original Local Binary Pattern (OLBP) feature map of an
    input color Point Cloud.

    Parameters:
        pcd (geometry.PointCloud): Input point cloud from open3d.
        neighbors (int): Number of LBP neighbors

    Returns:
        geometry.PointCloud: 3D feature map computed using OLBP algorithm.
    """
    return abstract_complete_lbp_iterator_feature_extract(
        pcd, neighbors, lower_cltp_center_module.iterator_lower_cltp_center
    )


def cltp(pcd: geometry.PointCloud, neighbors: int = 8) -> geometry.PointCloud:
    return (
        lower_cltp_center(pcd, neighbors),
        lower_cltp_magnitude(pcd, neighbors),
        lower_cltp_signs(pcd, neighbors),
        upper_cltp_center(pcd, neighbors),
        upper_cltp_magnitude(pcd, neighbors),
        upper_cltp_signs(pcd, neighbors),
    )


def cltp_feature(
    pcd: geometry.PointCloud, neighbors: int = 8
) -> npt.NDArray[np.int_]:
    """
    Computes the histogram of the Complete Local Binary Pattern (CLBP) from
    the feature map of an input color Point Cloud.

    Parameters:
        pcd (geometry.PointCloud): Input point cloud from open3d.
        neighbors (int): Number of LBP neighbors

    Returns:
        geometry.PointCloud: Histogram features of CLBP feature map.
    """
    maps = cltp(pcd, neighbors)
    lower_center, lower_magnitude, lower_signs = maps[0], maps[1], maps[2]
    upper_center, upper_magnitude, upper_signs = maps[3], maps[4], maps[5]

    histogram_lower_center = compute_feature_histogram(
        lower_center, number_bins=2
    )
    histogram_lower_magnitude = compute_feature_histogram(
        lower_magnitude, number_bins=2**neighbors
    )
    histogram_lower_signs = compute_feature_histogram(
        lower_signs, number_bins=2**neighbors
    )

    histogram_upper_center = compute_feature_histogram(
        upper_center, number_bins=2
    )
    histogram_upper_magnitude = compute_feature_histogram(
        upper_magnitude, number_bins=2**neighbors
    )
    histogram_upper_signs = compute_feature_histogram(
        upper_signs, number_bins=2**neighbors
    )

    return np.concatenate(
        (
            histogram_lower_center,
            histogram_lower_magnitude,
            histogram_lower_signs,
            histogram_upper_center,
            histogram_upper_magnitude,
            histogram_upper_signs,
        )
    )
