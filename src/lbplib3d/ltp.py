"""
Module: ltp.py
Author: Maria Luiza Macedo Bezerra
Date: May 29, 2024.
Description: This module provides functions for computing
local ternary patterns (LTP) feature maps.
"""

from typing import Tuple
import numpy as np
import numpy.typing as npt
from open3d import geometry
from open3d.cpu.pybind.geometry import PointCloud
from lbplib3d.extensions.rltp_module import rltp_module
from lbplib3d.extensions.lower_ltp_module import lower_ltp_module
from lbplib3d.extensions.upper_ltp_module import upper_ltp_module
from lbplib3d.extensions.dif_ltp_module import dif_ltp_module
from lbplib3d._helpers import (
    compute_feature_histogram,
    abstract_lbp_iterator_feature_extract,
)


def rltp(pcd: geometry.PointCloud, neighbors: int = 8) -> geometry.PointCloud:
    """
    Computes the Robust Local Ternary Pattern (LTP) feature map of an
    input color Point Cloud.

    Parameters:
        pcd (geometry.PointCloud): Input point cloud from open3d.
        neighbors (int): Number of LBP neighbors

    Returns:
        geometry.PointCloud: 3D feature map computed using RLTP algorithm.
    """
    return abstract_lbp_iterator_feature_extract(
        pcd, neighbors, rltp_module.iterator_rltp
    )


def lower_ltp(
    pcd: geometry.PointCloud, neighbors: int = 8
) -> geometry.PointCloud:
    """
    Computes the lower bound of the Local Ternary Pattern (Lower_LTP)
    feature map of an input color Point Cloud.

    Parameters:
        pcd (geometry.PointCloud): Input point cloud from open3d.
        neighbors (int): Number of LBP neighbors

    Returns:
        geometry.PointCloud: 3D feature map computed using LTP algorithm.
    """
    return abstract_lbp_iterator_feature_extract(
        pcd, neighbors, lower_ltp_module.iterator_lower_ltp
    )


def upper_ltp(
    pcd: geometry.PointCloud, neighbors: int = 8
) -> geometry.PointCloud:
    """
    Computes the Upper bound of the Local Ternary Pattern (Upper_LTP)
    feature map of an input color Point Cloud.

    Parameters:
        pcd (geometry.PointCloud): Input point cloud from open3d.
        neighbors (int): Number of LBP neighbors

    Returns:
        geometry.PointCloud: 3D feature map computed using LTP algorithm.
    """
    return abstract_lbp_iterator_feature_extract(
        pcd, neighbors, upper_ltp_module.iterator_upper_ltp
    )


def dif_ltp(
    pcd: geometry.PointCloud, neighbors: int = 8
) -> geometry.PointCloud:
    """
    Computes the the difference between the upper and lower bounds of the
    Local Ternary Pattern (Dif_LTP) feature map of an input color Point Cloud.

    Parameters:
        pcd (geometry.PointCloud): Input point cloud from open3d.
        neighbors (int): Number of LBP neighbors

    Returns:
        geometry.PointCloud: 3D feature map computed using LTP algorithm.
    """
    return abstract_lbp_iterator_feature_extract(
        pcd, neighbors, dif_ltp_module.iterator_dif_ltp
    )


def ltp(
    pcd: geometry.PointCloud, neighbors: int = 8
) -> Tuple[PointCloud, PointCloud, PointCloud, PointCloud]:
    return (
        lower_ltp(pcd, neighbors),
        upper_ltp(pcd, neighbors),
        dif_ltp(pcd, neighbors),
        rltp(pcd, neighbors),
    )


def ltp_feature(
    pcd: geometry.PointCloud, neighbors: int = 8
) -> npt.NDArray[np.int_]:
    """
    Computes the histogram of the Local Ternary Pattern (LTP) from
    the feature map of an input color Point Cloud.

    Parameters:
        pcd (geometry.PointCloud): Input point cloud from open3d.
        neighbors (int): Number of LBP neighbors

    Returns:
        geometry.PointCloud: Histogram features of CLBP feature map.
    """
    lower, upper, diff, rltp = ltp(pcd, neighbors)

    histogram_lower = compute_feature_histogram(
        lower, number_bins=2**neighbors
    )
    histogram_upper = compute_feature_histogram(
        upper, number_bins=2**neighbors
    )
    histogram_diff = compute_feature_histogram(diff, number_bins=2**neighbors)
    histogram_rltp = compute_feature_histogram(rltp, number_bins=2**neighbors)

    return np.concatenate(
        (histogram_lower, histogram_upper, histogram_diff, histogram_rltp)
    )
