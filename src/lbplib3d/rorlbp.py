"""
Module: rorlbp.py
Author: Maria Luiza Macedo
Date: May 29, 2024.
Description: This module provides functions for computing the
Rotation-Invariant Local Binary Pattern (RORLBP) feature maps.
"""

import numpy as np
import numpy.typing as npt
import open3d as o3d
from lbplib3d.extensions.rorlbp_module import rorlbp_module
from pandas.io.formats.info import show_counts_sub

from lbplib3d._helpers import compute_feature_histogram
from lbplib3d._helpers import abstract_lbp_iterator_feature_extract


def rorlbp(
    pcd: o3d.geometry.PointCloud, neighbors: int = 8
) -> o3d.geometry.PointCloud:
    """
    Computes the extension of default LBP pattern which is grayscale invariant
    and rotation invariant to generate the feature map of an input color
    Point Cloud.

    Parameters:
        pcd (o3d.geometry.PointCloud): Input point cloud from open3d.
        neighbors (int): Number of LBP neighbors

    Returns:
        o3d.geometry.PointCloud: 3D feature map computed using rorlbp
            algorithm.
    """
    return abstract_lbp_iterator_feature_extract(
        pcd, neighbors, rorlbp_module.iterator_rorlbp
    )


def rorlbp_feature(
    pcd: o3d.geometry.PointCloud, neighbors: int = 8
) -> npt.NDArray[np.int_]:
    """
    Computes the histogram of the grayscale invariant and rotation invariant
    LBP (RORLBP) from the feature map of an input color Point Cloud.

    Parameters:
        pcd (o3d.geometry.PointCloud): Input point cloud from open3d.
        neighbors (int): Number of LBP neighbors

    Returns:
        o3d.geometry.PointCloud: Histogram features of RORLBP feature map.
    """
    feature_map = rorlbp(pcd, neighbors)
    return compute_feature_histogram(feature_map, number_bins=2**neighbors)
