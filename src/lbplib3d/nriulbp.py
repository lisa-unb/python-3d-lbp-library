"""
Module: nriulbp.py
Author: Maria Luiza Macedo
Date: May 29, 2024.
Description: This module provides functions for computing the Uniform
local binary pattern (ULBP) feature maps.
"""

import numpy as np
import numpy.typing as npt
import open3d as o3d
from lbplib3d.extensions.nriulbp_module import nriulbp_module
from lbplib3d._helpers import compute_feature_histogram
from lbplib3d._helpers import abstract_lbp_iterator_feature_extract


def nriulbp(
    pcd: o3d.geometry.PointCloud, neighbors: int = 8
) -> o3d.geometry.PointCloud:
    """
    Computes the Non-Rotation-Invariant Uniform Local Binary Pattern (NRIULBP)
    feature map of an input color Point Cloud.

    Parameters:
        pcd (o3d.geometry.PointCloud): Input point cloud from open3d.
        neighbors (int): Number of LBP neighbors

    Returns:
        o3d.geometry.PointCloud: 3D feature map computed using NRIULBP.
    """
    return abstract_lbp_iterator_feature_extract(
        pcd, neighbors, nriulbp_module.iterator_nriulbp
    )


def nriulbp_feature(
    pcd: o3d.geometry.PointCloud, neighbors: int = 8
) -> npt.NDArray[np.int_]:
    """
    Computes the histogram of the Non-Rotation-Invariant Uniform Local Binary
    Pattern (NRIULBP)  from the feature map of an input color Point Cloud.

    Parameters:
        pcd (o3d.geometry.PointCloud): Input point cloud from open3d.
        neighbors (int): Number of LBP neighbors

    Returns:
        o3d.geometry.PointCloud: Histogram features of NRIULBP feature map.
    """
    feature_map = nriulbp(pcd, neighbors)
    bins = neighbors * (neighbors + 1) + 2
    return compute_feature_histogram(feature_map, number_bins=bins)
