"""
Module: ulbp.py
Author: Maria Luiza Macedo
Date: May 29, 2024.
Description: This module provides functions for computing the Uniform
local binary pattern (ULBP) feature maps.
"""

import numpy as np
import numpy.typing as npt
import open3d as o3d
from lbplib3d.extensions.ulbp_module import ulbp_module
from lbplib3d._helpers import compute_feature_histogram
from lbplib3d._helpers import abstract_lbp_iterator_feature_extract


def ulbp(
    pcd: o3d.geometry.PointCloud, neighbors: int = 8
) -> o3d.geometry.PointCloud:
    """
    Computes the Uniform Local Binary Pattern (ULBP) feature map of an
    input color Point Cloud.

    Parameters:
        pcd (o3d.geometry.PointCloud): Input point cloud from open3d.
        neighbors (int): Number of LBP neighbors

    Returns:
        o3d.geometry.PointCloud: 3D feature map computed using ULBP algorithm.
    """
    return abstract_lbp_iterator_feature_extract(
        pcd, neighbors, ulbp_module.iterator_ulbp
    )


def ulbp_feature(
    pcd: o3d.geometry.PointCloud, neighbors: int = 8
) -> npt.NDArray[np.int_]:
    """
    Computes the histogram of the Uniform Local Binary Pattern (ULBP) from
    the feature map of an input color Point Cloud.

    Parameters:
        pcd (o3d.geometry.PointCloud): Input point cloud from open3d.
        neighbors (int): Number of LBP neighbors

    Returns:
        o3d.geometry.PointCloud: Histogram features of OLBP feature map.
    """
    feature_map = ulbp(pcd, neighbors)
    bins = neighbors + 2
    return compute_feature_histogram(feature_map, number_bins=bins)
