!-----------------------------------------------------------------------
! Module: ulbp_descriptor
! Author: Maria Luiza Macedo Bezerra
! Date: May 29, 2024.
! Description: This code implements the local operations to compute the
!              Uniform 3D Local Binary Pattern (ULBP) descriptor using
!              a target (point) and their nearest neighbors.
!-----------------------------------------------------------------------
module ulbp_descriptor
   implicit none

contains

   real function ulbp_descriptor_function(target_point, neighbors)
      use common
      use kdtree
      implicit none
      ! Input/Interface function
      type(Point), intent(in) :: target_point
      type(Neighbor), allocatable, intent(in) :: neighbors(:)
      ! Specific variable functions
      integer :: i
      integer :: P
      integer :: changes
      integer :: lbp
      integer, dimension(size(neighbors)) :: signed_texture
      real :: center, neighbor_point

      P = size(neighbors)
      center = target_point%color(1)
      do i = 1, P
         neighbor_point = neighbors(i)%point_%color(1)
         !&<
         signed_texture(i) = check_if_diff_bigger_than_zero(&
                 &center, neighbor_point)
         !&>
      end do

      changes = computer_uniform_changes(signed_texture)

      lbp = 0
      if (changes <= 2) then
         do i = 1, P
            lbp = lbp + signed_texture(i)
         end do
      else
         lbp = P + 1
      end if

      ulbp_descriptor_function = lbp
   end function ulbp_descriptor_function

end module ulbp_descriptor
