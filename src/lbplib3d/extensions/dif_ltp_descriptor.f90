!-----------------------------------------------------------------------
! Module: dif_ltp_descriptor
! Author: Maria Luiza Macedo Bezerra
! Date: May 20, 2024.
! Description: This code implements the local operations to compute the
!              3D difference between the upper and lower bounds of the
!              Local Ternary Pattern (dif_LTP) descriptor using the upper
!              and lower bounds of the LTP
!-----------------------------------------------------------------------
module dif_ltp_descriptor
   implicit none

contains

   real function dif_ltp_descriptor_function(target_point, neighbors)
      use common
      use kdtree
      use upper_ltp_descriptor
      use lower_ltp_descriptor
      implicit none
      ! Input/Interface function
      type(Point), intent(in) :: target_point
      type(Neighbor), allocatable, intent(in) :: neighbors(:)
      ! Specific variable functions
      integer :: code_lower, code_upper

      code_upper = upper_ltp_descriptor_function(target_point, neighbors)
      code_lower = lower_ltp_descriptor_function(target_point, neighbors)
      dif_ltp_descriptor_function = abs(code_upper - code_lower)

   end function dif_ltp_descriptor_function

end module dif_ltp_descriptor

