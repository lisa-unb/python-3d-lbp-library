!-----------------------------------------------------------------------
! Module: lower_cltp_magnitude_descriptor
! Author: Maria Luiza Macedo Bezerra
! Date: May 20, 2024.
! Description: This code implements the local operations to compute the
!              lower bound of the magnitude part of compleate local ternary 
!              pattern(CLTP) descriptor using a target (point)
!              and their nearest neighbors.
!-----------------------------------------------------------------------
module lower_cltp_magnitude_descriptor
   implicit none

contains

   !&<
   real function lower_cltp_magnitude_descriptor_function( &
           target_point, &
           neighbors, &
           mean_value, &
           mean_mp_value)
      !&>
      use common
      use kdtree
      implicit none
      ! Input/Interface function
      type(Point), intent(in) :: target_point
      type(Neighbor), allocatable, intent(in) :: neighbors(:)
      real, intent(in) :: mean_value, mean_mp_value
      ! Specific variable functions
      integer :: n, code
      real :: center, point_color, magnitude_operator, treshold

      center = target_point%color(1)*255
      code = 0
      treshold = 6.0
      do n = 1, size(neighbors)
         point_color = 255*neighbors(n)%point_%color(1)

         magnitude_operator = abs(point_color - (center - treshold))
         code = IOR(code, IBSET(0, n - 1)*&
            & logical_to_integer(magnitude_operator >= (255*mean_mp_value)))

      end do
      lower_cltp_magnitude_descriptor_function = code
   end function lower_cltp_magnitude_descriptor_function

end module lower_cltp_magnitude_descriptor
