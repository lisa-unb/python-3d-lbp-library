!-----------------------------------------------------------------------
! Module: nriulbp_descriptor
! Author: Pedro Garcia Freitas
! Date: Jun 20, 2024.
! Description: This code implements a variant of uniform pattern which is
!              grayscale invariant but not rotation invariant
!-----------------------------------------------------------------------
module nriulbp_descriptor
   implicit none

contains

   real function nriulbp_descriptor_function(target_point, neighbors)
      use common
      use kdtree
      implicit none
      ! Input/Interface function
      type(Point), intent(in) :: target_point
      type(Neighbor), allocatable, intent(in) :: neighbors(:)
      ! Specific variable functions
      integer :: i
      integer :: P
      integer :: changes
      integer :: n_ones, first_zero, first_one, rot_index
      integer :: lbp
      integer, dimension(size(neighbors)) :: signed_texture
      real :: center, neighbor_point

      P = size(neighbors)
      center = target_point%color(1)
      do i = 1, P
         neighbor_point = neighbors(i)%point_%color(1)
         signed_texture(i) = check_if_diff_bigger_than_zero(&
                 &center, neighbor_point)
      end do

      changes = computer_uniform_changes(signed_texture)

      lbp = 0
      if (changes <= 2) then
         ! changes <= 2 means a uniform pattern
         n_ones = 0      ! determines the number of ones
         first_one = -1  ! position was the first one
         first_zero = -1 ! position of the first zero
         do i = 1, P
            if (signed_texture(i) /= 0) then
               n_ones = n_ones + 1
               if (first_one == -1) then
                  first_one = i
               end if
            else
               if (first_one == -1) then
                  first_one = i
               end if
            end if
         end do

         if (n_ones == 0) then
            lbp = 0
         else if (n_ones == P) then
            lbp = P*(P - 1) + 1
         else
            if (first_one == 0) then
               rot_index = n_ones - first_zero
            else
               rot_index = P - first_one
            end if
            lbp = 1 + (n_ones - 1)*P + rot_index
         end if
      else
         lbp = P*(P + 1) + 2
      end if

      nriulbp_descriptor_function = lbp
   end function nriulbp_descriptor_function

end module nriulbp_descriptor
