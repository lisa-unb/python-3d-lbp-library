!-----------------------------------------------------------------------
! Module: CLBP_signs_descriptor
! Author: Maria Luiza Macedo Bezerra
! Date: May 29, 2024.
! Description: This code implements the local operations to compute the
!              signs part of the complete 3D local binary pattern(CLBP)
!              descriptor using a target (point) and their nearest neighbors.
!-----------------------------------------------------------------------
module clbp_signs_descriptor
   implicit none

contains

   !&<
   real function clbp_signs_descriptor_function( &
           target_point, &
           neighbors, &
           mean_value, &
           mean_mp_value)
      !&>
      use common
      use kdtree
      implicit none
      ! Input/Interface function
      type(Point), intent(in) :: target_point
      type(Neighbor), allocatable, intent(in) :: neighbors(:)
      real, intent(in) :: mean_value, mean_mp_value
      ! Specific variable functions
      integer :: code_signs, n
      real :: center, point_color

      center = target_point%color(1)
      code_signs = 0

      do n = 1, size(neighbors)
         point_color = neighbors(n)%point_%color(1)
         code_signs = IOR(code_signs, IBSET(0, n - 1)*&
            & logical_to_integer(point_color >= center))
      end do
      clbp_signs_descriptor_function = code_signs
   end function clbp_signs_descriptor_function

end module clbp_signs_descriptor
