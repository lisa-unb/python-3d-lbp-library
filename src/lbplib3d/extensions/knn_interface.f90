module knn_interface
   use, intrinsic :: iso_c_binding
   implicit none

   interface
      subroutine build_kdtree(points, n_points) bind(C, name="build_kdtree")
         import :: c_float, c_int
         real(c_float), dimension(*) :: points
         integer(c_int), value :: n_points
      end subroutine build_kdtree

      subroutine knn_search(query, k, indices, distances) bind(C, name="knn_search")
         import :: c_float, c_int
         real(c_float), dimension(*) :: query
         integer(c_int), value :: k
         integer(c_int), dimension(*) :: indices
         real(c_float), dimension(*) :: distances
      end subroutine knn_search
   end interface
end module knn_interface
