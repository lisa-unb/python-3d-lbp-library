!-----------------------------------------------------------------------
! Module: ulbp_module
! Author: Maria Luiza Macedo Bezerra
! Date: May 29, 2024.
! Description: This module provides basic operations to compute the
!              Uniform 3D Local Binary Pattern (ULBP). This code is
!!              an adaptation for 3D of the descriptor described in
!              https://doi.org/10.1109/TPAMI.2002.1017623 (see Eq.9)
!-----------------------------------------------------------------------
module ulbp_module
   implicit none

contains

   !&<
   subroutine iterator_ulbp(&
           &geometry, rows, cols, &
           &color, rows_color, cols_color, &
           &number_of_neighbors, &
           &map_labels)
      !&>
      use abstract_descriptor
      use ulbp_descriptor

      implicit none
      ! Inputs
      integer, intent(in) :: rows, cols
      real, dimension(rows, cols), intent(in) :: geometry
      integer, intent(in) :: rows_color, cols_color
      real, dimension(rows_color, cols_color), intent(in) :: color
      integer, intent(in) :: number_of_neighbors
      ! Outputs
      integer, dimension(rows_color, cols_color), intent(out) :: map_labels

      call abstract_iterator(&
              &descriptor_function=ulbp_descriptor_function, &
              &geometry=geometry, rows=rows, cols=cols, &
              &color=color, rows_color=rows_color, cols_color=cols_color, &
              &number_of_neighbors=number_of_neighbors, &
              &map_labels=map_labels)
   end subroutine iterator_ulbp

end module ulbp_module
