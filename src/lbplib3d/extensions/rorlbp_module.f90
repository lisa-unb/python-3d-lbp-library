!-----------------------------------------------------------------------
! Module: rorlbp_module
! Author: Pedro Garcia Freitas
! Date: July 01, 2024.
! Description: This module provides basic operations to compute the
!              extension of default LBP pattern which is grayscale invariant
!              and an adaptation for 3D of the descriptor described in
!-----------------------------------------------------------------------
module rorlbp_module
   implicit none

contains

   !&<
   subroutine iterator_rorlbp(&
           &geometry, rows, cols, &
           &color, rows_color, cols_color, &
           &number_of_neighbors, &
           &map_labels)
      !&>
      use abstract_descriptor
      use rorlbp_descriptor

      implicit none
      ! Inputs
      integer, intent(in) :: rows, cols
      real, dimension(rows, cols), intent(in) :: geometry
      integer, intent(in) :: rows_color, cols_color
      real, dimension(rows_color, cols_color), intent(in) :: color
      integer, intent(in) :: number_of_neighbors
      ! Outputs
      integer, dimension(rows_color, cols_color), intent(out) :: map_labels

      !&<
      call abstract_iterator(&
              &descriptor_function=rorlbp_descriptor_function, &
              &geometry=geometry, rows=rows, cols=cols, &
              &color=color, rows_color=rows_color, cols_color=cols_color, &
              &number_of_neighbors=number_of_neighbors, &
              &map_labels=map_labels)
      !&>
   end subroutine iterator_rorlbp

end module rorlbp_module
