!-----------------------------------------------------------------------
! Module: abstract_descriptor.f90
! Author: Pedro Garcia Freitas
! Date: May 20, 2024.
! Description: This module provides basic operations to compute the
!              3D Local Binary Pattern (LBP) descriptor and its derivatives
!-----------------------------------------------------------------------
module commons_abstract_descriptors
   implicit none
contains
   subroutine init_points(geometry, color, points)
      use kdtree
      implicit none
      ! Input-Only
      real, dimension(:, :), intent(in) :: geometry
      real, dimension(:, :), intent(in) :: color
      ! Input/Output functions
      type(Point), dimension(:), intent(inout) :: points
      ! Function variables
      integer :: rows
      integer :: i

      rows = size(points, dim=1)
      !$OMP PARALLEL DO PRIVATE(i) SHARED(points, geometry, color)
      do i = 1, rows
         points(i)%coord(1) = geometry(i, 1)
         points(i)%coord(2) = geometry(i, 2)
         points(i)%coord(3) = geometry(i, 3)
         points(i)%color(1) = color(i, 1)
         if (size(color, 2) > 1) then
            points(i)%color(2) = color(i, 2)
            points(i)%color(3) = color(i, 3)
         else
            points(i)%color(2) = color(i, 1)
            points(i)%color(3) = color(i, 1)
         end if
      end do
      !$OMP END PARALLEL DO
   end subroutine init_points
end module commons_abstract_descriptors

module abstract_descriptor
   implicit none

   ! Define abstract interface for the descriptions function to be implemented.
   abstract interface
      real function abstract_descriptor_function(target_point, neighbors)
         use kdtree
         type(Point), intent(in) :: target_point
         type(Neighbor), allocatable, intent(in) :: neighbors(:)
      end function abstract_descriptor_function
   end interface

contains

   !&<
   subroutine abstract_iterator(&
           &descriptor_function, &
           &geometry, rows, cols, &
           &color, rows_color, cols_color, &
           &number_of_neighbors, &
           &map_labels)
      !&>
      use common
      use omp_lib
      use kdtree
      use commons_abstract_descriptors
      implicit none
      ! Inputs
      procedure(abstract_descriptor_function) :: descriptor_function
      integer, intent(in) :: rows, cols
      real, dimension(rows, cols), intent(in) :: geometry
      integer, intent(in) :: rows_color, cols_color
      real, dimension(rows_color, cols_color), intent(in) :: color
      integer, intent(in) :: number_of_neighbors
      ! Outputs
      integer, dimension(rows_color, cols_color), intent(out) :: map_labels
      ! Function variables
      integer :: i, j
      real :: code
      ! KDTree variables
      type(Point), dimension(rows) :: points
      ! Variables to be passed to abstract function
      type(Point) :: target_point
      type(Neighbor), allocatable :: neighbors(:)

      call init_points(geometry, color, points)

      ! Build the KD-tree
      call build(points)

      !$OMP PARALLEL DO PRIVATE(rows, neighbors, target_point, code) &
      !$OMP SHARED(map_labels, points, number_of_neighbors)
      do i = 1, rows
         target_point = points(i)
         !&<
         call find_nearest( &
                 target_point, &
                 points, &
                 neighbors, &
                 number_of_neighbors)
         !&>

         code = descriptor_function(target_point, neighbors)

         do j = 1, cols_color
            map_labels(i, j) = code
         end do
      end do
      !$OMP END PARALLEL DO

      if (allocated(neighbors)) deallocate (neighbors)
   end subroutine abstract_iterator

end module abstract_descriptor

module complete_abstract_descriptor
   implicit none

   ! Define abstract interface for the descriptions function to be implemented.
   abstract interface
      !&<
      real function complete_abstract_descriptor_function(&
              &target_point, &
              &neighbors, &
              &mean_value, &
              &mean_mp_value)
         use kdtree
         type(Point), intent(in) :: target_point
         type(Neighbor), allocatable, intent(in) :: neighbors(:)
         real, intent(in) :: mean_value, mean_mp_value
      end function complete_abstract_descriptor_function
      !&>
   end interface

contains

   !&<
   subroutine complete_abstract_iterator(&
           &descriptor_function, &
           &geometry, rows, cols, &
           &color, rows_color, cols_color, &
           &number_of_neighbors, &
           &map_labels,&
           & mean_value, &
           &gray_colors_size)
      !&>
      use common
      use omp_lib
      use kdtree
      use commons_abstract_descriptors
      implicit none
      ! Inputs
      procedure(complete_abstract_descriptor_function) :: descriptor_function
      integer, intent(in) :: rows, cols
      real, dimension(rows, cols), intent(in) :: geometry
      integer, intent(in) :: rows_color, cols_color
      real, dimension(rows_color, cols_color), intent(in) :: color
      integer, intent(in) :: number_of_neighbors, gray_colors_size
      real, intent(in) :: mean_value
      ! Outputs
      integer, dimension(rows_color, cols_color), intent(out) :: map_labels
      ! Function variables
      integer :: i, j, n
      real :: code, difference, point_color, center, mean_mp_value, d
      ! KDTree variables
      type(Point), dimension(rows) :: points
      ! Variables to be passed to abstract function
      type(Point) :: target_point
      type(Neighbor), allocatable :: neighbors(:)

      call init_points(geometry, color, points)

      ! Build the KD-tree
      call build(points)

      !calculating the average m_p value, for calculating clbp
      difference = 0.0
      mean_mp_value = 0.0
      do i = 1, rows
         target_point = points(i)
         !&<
         call find_nearest( &
                 target_point, &
                 points, &
                 neighbors, &
                 number_of_neighbors)
         !&>

         do n = 1, number_of_neighbors
            center = target_point%color(1)
            point_color = neighbors(n)%point_%color(1)
            d = abs((center - point_color)/number_of_neighbors)
            difference = difference + d
         end do
      end do
      mean_mp_value = difference/gray_colors_size

      !$OMP PARALLEL DO PRIVATE(rows, neighbors, target_point, code) &
      !$OMP SHARED(map_labels, points, number_of_neighbors)
      do i = 1, rows
         target_point = points(i)
         !&<
         call find_nearest( &
                 target_point, &
                 points, &
                 neighbors, &
                 number_of_neighbors)
         !&>

         !&<
         code = descriptor_function(&
                 &target_point, &
                 &neighbors, &
                 &mean_value, &
                 &mean_mp_value)
         !&>

         do j = 1, cols_color
            map_labels(i, j) = code
         end do
      end do
      !$OMP END PARALLEL DO

      if (allocated(neighbors)) deallocate (neighbors)
   end subroutine complete_abstract_iterator

end module complete_abstract_descriptor

