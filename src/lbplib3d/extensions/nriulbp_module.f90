!-----------------------------------------------------------------------
! Module: nriulbp_module
! Author: Pedro Garcia Freitas
! Date: Jun 20, 2024.
! Description: Variant of uniform pattern which is grayscale invariant
!              but not rotation invariant. For details, see:
!              * https://doi.org/10.1007/978-3-540-24670-1_3
!              * https://doi.org/10.1109/TPAMI.2006.244
!-----------------------------------------------------------------------
module nriulbp_module
   implicit none

contains

   !&<
   subroutine iterator_nriulbp(&
           &geometry, rows, cols, &
           &color, rows_color, cols_color, &
           &number_of_neighbors, &
           &map_labels)
      !&>
      use abstract_descriptor
      use nriulbp_descriptor

      implicit none
      ! Inputs
      integer, intent(in) :: rows, cols
      real, dimension(rows, cols), intent(in) :: geometry
      integer, intent(in) :: rows_color, cols_color
      real, dimension(rows_color, cols_color), intent(in) :: color
      integer, intent(in) :: number_of_neighbors
      ! Outputs
      integer, dimension(rows_color, cols_color), intent(out) :: map_labels

      !&<
      call abstract_iterator(&
              &descriptor_function=nriulbp_descriptor_function, &
              &geometry=geometry, rows=rows, cols=cols, &
              &color=color, rows_color=rows_color, cols_color=cols_color, &
              &number_of_neighbors=number_of_neighbors, &
              &map_labels=map_labels)
      !&>
   end subroutine iterator_nriulbp

end module nriulbp_module
