!-----------------------------------------------------------------------
! Module: CLBP_center_module
! Author: Maria Luiza Macedo Bezerra
! Date: May 29, 2024.
! Description: This module provides basic operations to compute the
!              center part of the complete 3D local binary pattern(CLBP).
!              This code is an adaptation for 3D of the descriptor described in
!              https://doi.org/10.1109/TPAMI.2002.1017623 (see Eq.9)
!-----------------------------------------------------------------------
module clbp_center_module
   implicit none

contains

   !&<
   subroutine iterator_clbp_center(&
           &geometry, rows, cols, &
           &color, rows_color, cols_color, &
           &number_of_neighbors, &
           &map_labels, &
           &mean_value, &
           & gray_colors_size)
      !&>
      use complete_abstract_descriptor
      use clbp_center_descriptor

      implicit none
      ! Inputs
      integer, intent(in) :: rows, cols
      real, dimension(rows, cols), intent(in) :: geometry
      integer, intent(in) :: rows_color, cols_color
      real, dimension(rows_color, cols_color), intent(in) :: color
      integer, intent(in) :: number_of_neighbors, gray_colors_size
      real, intent(in) :: mean_value
      ! Outputs
      integer, dimension(rows_color, cols_color), intent(out) :: map_labels

      call complete_abstract_iterator(&
              &descriptor_function=clbp_center_descriptor_function, &
              &geometry=geometry, rows=rows, cols=cols, &
              &color=color, rows_color=rows_color, cols_color=cols_color, &
              &number_of_neighbors=number_of_neighbors, &
              &map_labels=map_labels, &
              &mean_value=mean_value, &
              &gray_colors_size=gray_colors_size)
   end subroutine iterator_clbp_center

end module clbp_center_module
