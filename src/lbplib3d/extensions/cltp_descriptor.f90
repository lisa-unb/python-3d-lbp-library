!-----------------------------------------------------------------------
! Module: cltp_descriptor
! Author: Maria Luiza Macedo Bezerra
! Date: May 20, 2024.
! Description: This code implements the local operations to compute the
!              3D Robust Local Ternary Pattern (RLTP) descriptor using
!              a target (point) and their nearest neighbors.
!-----------------------------------------------------------------------
module cltp_descriptor
   implicit none

contains

   real function cltp_descriptor_function(target_point, neighbors)
      use common
      use kdtree
      implicit none
      ! Input/Interface function
      type(Point), intent(in) :: target_point
      type(Neighbor), allocatable, intent(in) :: neighbors(:)
      ! Specific variable functions
      integer :: n, treshold, difference
      integer :: code
      real :: center, point_color

      center = target_point%color(1)
      code = 0
      treshold = 6
      do n = 1, size(neighbors)
         point_color = neighbors(n)%point_%color(1)
         difference = point_color - center

         if (difference >= treshold) then
            code = code + 2*(3**(n - 1))

         else if (abs(difference) <= treshold) then
            code = code + (3**(n - 1))

         else if (difference <= -treshold) then
            code = code

         end if
      end do
      cltp_descriptor_function = abs(int(code*0.0388))
   end function cltp_descriptor_function

end module cltp_descriptor
