!-----------------------------------------------------------------------
! Module: cslbp_module
! Author: Maria Luiza Macedo Bezerra
! Date: august 14, 2024.
! Description: This code implements the local operations to compute
!              an center simetric local binary pattern (cslbp)inspired
!              descriptor using the distance vector of each point,
!              considering the center as reference.
!-----------------------------------------------------------------------
module cslbp_module
   implicit none

contains

   !&<
   subroutine iterator_cslbp(&
           &geometry, rows, cols, &
           &color, rows_color, cols_color, &
           &number_of_neighbors, &
           &map_labels)
      !&>
      use abstract_descriptor
      use cslbp_descriptor

      implicit none
      ! Inputs
      integer, intent(in) :: rows, cols
      real, dimension(rows, cols), intent(in) :: geometry
      integer, intent(in) :: rows_color, cols_color
      real, dimension(rows_color, cols_color), intent(in) :: color
      integer, intent(in) :: number_of_neighbors
      ! Outputs
      integer, dimension(rows_color, cols_color), intent(out) :: map_labels

      !&<
      call abstract_iterator(&
              &descriptor_function=cslbp_descriptor_function, &
              &geometry=geometry, rows=rows, cols=cols, &
              &color=color, rows_color=rows_color, cols_color=cols_color, &
              &number_of_neighbors=number_of_neighbors, &
              &map_labels=map_labels)
      !&>
   end subroutine iterator_cslbp

end module cslbp_module
