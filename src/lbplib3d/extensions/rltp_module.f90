!-----------------------------------------------------------------------
! Module: rltp_module
! Author: Pedro Garcia Freitas
! Date: May 17, 2024.
! Description: This module provides basic operations to compute the
!              3D Robust Local Ternary Pattern (RLTP) descriptor proposed
!              by Rafael Diniz, Pedro Garcia Freitas, Mylene CQ Farias
!              More details: https://doi.org/10.1109/QoMEX48832.2020.9123076
!-----------------------------------------------------------------------
module rltp_module
   implicit none

contains

   !&<
   subroutine iterator_rltp(&
           &geometry, rows, cols, &
           &color, rows_color, cols_color, &
           &number_of_neighbors, &
           &map_labels)
      !&>
      use abstract_descriptor
      use rltp_descriptor

      implicit none
      ! Inputs
      integer, intent(in) :: rows, cols
      real, dimension(rows, cols), intent(in) :: geometry
      integer, intent(in) :: rows_color, cols_color
      real, dimension(rows_color, cols_color), intent(in) :: color
      integer, intent(in) :: number_of_neighbors
      ! Outputs
      integer, dimension(rows_color, cols_color), intent(out) :: map_labels

      !&<
      call abstract_iterator(&
              &descriptor_function = rltp_descriptor_function, &
              &geometry = geometry, rows = rows, cols = cols, &
              &color = color, rows_color = rows_color, cols_color = cols_color, &
              &number_of_neighbors = number_of_neighbors, &
              &map_labels = map_labels)
      !&>
   end subroutine iterator_rltp

end module rltp_module
