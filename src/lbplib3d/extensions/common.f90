module common
   implicit none

contains
   function logical_to_integer(my_logical) result(my_integer)
      implicit none
      logical, intent(in) :: my_logical
      integer :: my_integer

      if (my_logical) then
         my_integer = 1
      else
         my_integer = 0
      end if
   end function logical_to_integer

   integer function check_if_diff_equals_zero(x, y)
      implicit none
      integer, intent(in) :: x, y
      check_if_diff_equals_zero = logical_to_integer((x - y) /= 0)
   end function check_if_diff_equals_zero

   integer function check_if_diff_bigger_than_zero(x, y)
      implicit none
      real, intent(in) :: x, y
      check_if_diff_bigger_than_zero = logical_to_integer((x - y) >= 0)
   end function check_if_diff_bigger_than_zero

   integer function computer_uniform_changes(signed_texture)
      implicit none
      ! Input/Interface function
      integer, dimension(:), intent(in) :: signed_texture
      ! function variables
      integer :: changes
      integer :: i

      do i = 1, size(signed_texture) - 1
         !&<
         changes = changes + check_if_diff_equals_zero(&
                 &signed_texture(i), signed_texture(i + 1))
         !&>
      end do
   end function computer_uniform_changes
end module common
