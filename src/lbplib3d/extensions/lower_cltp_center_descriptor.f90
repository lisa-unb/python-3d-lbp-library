!-----------------------------------------------------------------------
! Module: lower_cltp_center_descriptor
! Author: Maria Luiza Macedo Bezerra
! Date: May 20, 2024.
! Description: This code implements the local operations to compute the
!              lower bound of the center part of compleate local ternary 
!              pattern(CLTP) descriptor using a target (point)
!              and their nearest neighbors.
!-----------------------------------------------------------------------
module lower_cltp_center_descriptor
   implicit none

contains

   !&<
   real function lower_cltp_center_descriptor_function( &
           target_point, &
           neighbors, &
           mean_value, &
           mean_mp_value)
      !&>
      use common
      use kdtree
      implicit none
      ! Input/Interface function
      type(Point), intent(in) :: target_point
      type(Neighbor), allocatable, intent(in) :: neighbors(:)
      real, intent(in) :: mean_value, mean_mp_value
      ! Specific variable functions
      integer :: code
      real :: center, treshold, center_operator

      center = 255*target_point%color(1)
      code = 0
      treshold = 6.0
      center_operator = center - treshold
      code = 255*logical_to_integer(center_operator >= (255*mean_value))

      lower_cltp_center_descriptor_function = code
   end function lower_cltp_center_descriptor_function

end module lower_cltp_center_descriptor
