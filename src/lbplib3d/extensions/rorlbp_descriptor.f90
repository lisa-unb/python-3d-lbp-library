!-----------------------------------------------------------------------
! Module: ror_descriptor
! Author: Pedro Garcia Freitas
! Date: July 01, 2024.
! Description: This code implements the local operations to compute the
!              extension of default pattern which is grayscale invariant and
!              rotation invariant.
!-----------------------------------------------------------------------
module rorlbp_descriptor
   implicit none

contains

   real function rorlbp_descriptor_function(target_point, neighbors)
      use common
      use kdtree
      implicit none
      ! Input/Interface function
      type(Point), intent(in) :: target_point
      type(Neighbor), allocatable, intent(in) :: neighbors(:)
      ! Specific variable functions
      integer :: i
      integer :: P
      integer :: changes
      integer :: lbp
      integer, dimension(size(neighbors)) :: signed_texture
      integer, dimension(size(neighbors)) :: weights
      integer, dimension(size(neighbors)) :: rotation_chain
      real :: center, neighbor_point

      P = size(neighbors)
      rotation_chain = 0
      weights = compute_weights(P)
      center = target_point%color(1)
      do i = 1, P
         neighbor_point = neighbors(i)%point_%color(1)
         signed_texture(i) = check_if_diff_bigger_than_zero(&
                 &center, neighbor_point)
      end do

      changes = computer_uniform_changes(signed_texture)

      lbp = 0
      do i = 1, P
         lbp = lbp + signed_texture(i)*weights(i)
      end do

      rotation_chain(1) = lbp
      do i = 2, P
         rotation_chain(i) = bit_rotate_right(rotation_chain(i - 1), P)
      end do

      do i = 2, P
         lbp = min(lbp, rotation_chain(i))
      end do

      rorlbp_descriptor_function = lbp
   end function rorlbp_descriptor_function

   function bit_rotate_right(value, length) result(rotated_value)
      implicit none
      integer, intent(in) :: value, length
      integer :: rotated_value
      integer :: right_shifted, left_shifted, mask

      right_shifted = ishft(value, -1)
      mask = ibset(0, length - 1)
      left_shifted = iand(value, 1)*mask
      rotated_value = ior(right_shifted, left_shifted)
   end function bit_rotate_right

   function compute_weights(P) result(weights)
      integer, intent(in) :: P
      integer, dimension(P) :: weights
      integer :: i

      do i = 1, P
         weights(i) = 2**(i - 1)
      end do
   end function compute_weights
end module rorlbp_descriptor
