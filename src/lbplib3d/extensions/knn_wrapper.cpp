#include "nanoflann.hpp"
#include <cstdlib>
#include <memory>
#include <vector>

// Define a point cloud structure for nanoflann
struct PointCloud
{
  std::vector<std::array<float, 3>> pts;

  // Must return the number of data points
  inline size_t kdtree_get_point_count() const { return pts.size(); }

  // Returns the distance between the 'idx'-th point and the point 'p'
  inline float kdtree_distance(const float* p, const size_t idx, size_t) const
  {
    float d0 = p[0] - pts[idx][0];
    float d1 = p[1] - pts[idx][1];
    float d2 = p[2] - pts[idx][2];
    return d0 * d0 + d1 * d1 + d2 * d2;
  }

  // Returns the 'dim'-th coordinate of the 'idx'-th point in the data set
  inline float kdtree_get_pt(const size_t idx, int dim) const
  {
    return pts[idx][dim];
  }

  // Optional bounding-box computation
  template<class BBOX>
  bool kdtree_get_bbox(BBOX&) const
  {
    return false;
  }
};

// Global variables to hold the point cloud and KDTree
std::unique_ptr<PointCloud> global_cloud;
std::unique_ptr<nanoflann::KDTreeSingleIndexAdaptor<
  nanoflann::L2_Simple_Adaptor<float, PointCloud>,
  PointCloud,
  3>>
  global_tree;

// C++ function to build the KDTree
extern "C" void
build_kdtree(float* points, int n_points)
{
  global_cloud = std::make_unique<PointCloud>();
  global_cloud->pts.resize(n_points);

  // Copy points into the point cloud
  // Since Fortran uses column-major order, we need to transpose while copying
  for (int i = 0; i < n_points; ++i) {
    global_cloud->pts[i] = { points[i],
                             points[i + n_points],
                             points[i + 2 * n_points] };
  }

  // Build the KDTree
  global_tree = std::make_unique<nanoflann::KDTreeSingleIndexAdaptor<
    nanoflann::L2_Simple_Adaptor<float, PointCloud>,
    PointCloud,
    3>>(3, *global_cloud, nanoflann::KDTreeSingleIndexAdaptorParams(10));

  global_tree->buildIndex();
}

// C++ function to perform KNN search using the pre-built KDTree
extern "C" void
knn_search(float* query, int k, int* indices, float* distances)
{
  std::vector<size_t> ret_indexes(k);
  std::vector<float> out_dists_sqr(k);

  nanoflann::KNNResultSet<float> resultSet(k);
  resultSet.init(&ret_indexes[0], &out_dists_sqr[0]);
  global_tree->findNeighbors(resultSet, query, nanoflann::SearchParameters(10));

  // Copy results to output arrays
  for (int i = 0; i < k; ++i) {
    indices[i] = ret_indexes[i];
    distances[i] = out_dists_sqr[i];
  }
}
