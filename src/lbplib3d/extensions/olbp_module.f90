!-----------------------------------------------------------------------
! Module: olbp_module
! Author: Pedro Garcia Freitas
! Date: May 17, 2024.
! Description: This module provides basic operations to compute the
!              Original 3D Local Binary Pattern (OLBP) descriptor proposed
!              by Rafael Diniz, Pedro Garcia Freitas, Mylene CQ Farias
!              More details: https://doi.org/10.1109/QoMEX48832.2020.9123076
!-----------------------------------------------------------------------
module olbp_module
   implicit none

contains

   !&<
   subroutine iterator_olbp(&
           &geometry, rows, cols, &
           &color, rows_color, cols_color, &
           &number_of_neighbors, &
           &map_labels)
      !&>
      use abstract_descriptor
      use olbp_descriptor

      implicit none
      ! Inputs
      integer, intent(in) :: rows, cols
      real, dimension(rows, cols), intent(in) :: geometry
      integer, intent(in) :: rows_color, cols_color
      real, dimension(rows_color, cols_color), intent(in) :: color
      integer, intent(in) :: number_of_neighbors
      ! Outputs
      integer, dimension(rows_color, cols_color), intent(out) :: map_labels

      !&<
      call abstract_iterator(&
              &descriptor_function=olbp_descriptor_function, &
              &geometry=geometry, rows=rows, cols=cols, &
              &color=color, rows_color=rows_color, cols_color=cols_color, &
              &number_of_neighbors=number_of_neighbors, &
              &map_labels=map_labels)
      !&>
   end subroutine iterator_olbp

end module olbp_module
