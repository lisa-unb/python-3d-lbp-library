module KDTree
   use knn_interface
   implicit none

   type :: Point
      real :: coord(3)  ! Coordinates of the point with x, y, z coordinates
      real :: color(3)  ! Color of the point with R, G, B channels per point
   end type Point

   type :: Neighbor
      type(Point) :: point_
      real :: distance
   end type Neighbor

contains

   subroutine build(points)
      type(Point), intent(in) :: points(:)
      real(c_float), dimension(size(points), 3) :: geometry
      integer :: i
      integer(c_int) :: number_of_points

      do i = 1, size(points)
         geometry(i, 1) = points(i)%coord(1)
         geometry(i, 2) = points(i)%coord(2)
         geometry(i, 3) = points(i)%coord(3)
      end do

      number_of_points = size(points)
      call build_kdtree(geometry, number_of_points)
   end subroutine build

   subroutine find_nearest(query_point, points, neighbors, k)
      ! Inputs
      type(Point), intent(in) :: query_point
      type(Point), intent(in) :: points(:)
      integer, intent(in) :: k
      ! Outputs
      type(Neighbor), allocatable, intent(inout) :: neighbors(:)
      ! Function variables
      integer(c_int), dimension(k + 1) :: indices
      real(c_float), dimension(k + 1) :: distances
      real(c_float), dimension(3) :: query_geometry
      integer :: i

      query_geometry(1) = query_point%coord(1)
      query_geometry(2) = query_point%coord(2)
      query_geometry(3) = query_point%coord(3)

      call knn_search(query_geometry, k + 1, indices, distances)

      if (.not. allocated(neighbors)) allocate (neighbors(k))

      do i = 1, k
         neighbors(i)%point_ = points(indices(i + 1) + 1)
         neighbors(i)%distance = distances(i)
      end do
   end subroutine find_nearest

end module KDTree
