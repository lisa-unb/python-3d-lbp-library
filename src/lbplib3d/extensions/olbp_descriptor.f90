!-----------------------------------------------------------------------
! Module: olbp_descriptor
! Author: Pedro Garcia Freitas
! Date: May 20, 2024.
! Description: This code implements the local operations to compute the
!              Original 3D Local Binary Pattern (OLBP) descriptor using
!              a target (point) and their nearest neighbors.
!-----------------------------------------------------------------------
module olbp_descriptor
   implicit none

contains

   real function olbp_descriptor_function(target_point, neighbors)
      use common
      use kdtree
      implicit none
      ! Input/Interface function
      type(Point), intent(in) :: target_point
      type(Neighbor), allocatable, intent(in) :: neighbors(:)
      ! Specific variable functions
      integer :: n
      integer :: code
      real :: center, point_color

      center = target_point%color(1)
      code = 0
      do n = 1, size(neighbors)
         point_color = neighbors(n)%point_%color(1)
         !&<
         code = IOR(code, IBSET(0, n - 1) * &
                 &logical_to_integer(point_color > center))
         !&>
      end do
      olbp_descriptor_function = code
   end function olbp_descriptor_function

end module olbp_descriptor
