!-----------------------------------------------------------------------
! Module: CLBP_center_descriptor
! Author: Maria Luiza Macedo Bezerra
! Date: May 29, 2024.
! Description: This code implements the local operations to compute the
!              center part of the complete 3D local binary pattern(CLBP)
!              descriptor using a target (point) and their nearest neighbors.
!-----------------------------------------------------------------------
module clbp_center_descriptor
   implicit none

contains

   !&<
   real function clbp_center_descriptor_function( &
           target_point, &
           neighbors, &
           mean_value, &
           mean_mp_value)
      !&>
      use common
      use kdtree
      implicit none
      ! Input/Interface function
      type(Point), intent(in) :: target_point
      type(Neighbor), allocatable, intent(in) :: neighbors(:)
      real, intent(in) :: mean_value, mean_mp_value
      ! Specific variable functions
      integer :: code_center
      real :: center

      center = target_point%color(1)
      code_center = 255*logical_to_integer(center >= mean_value)
      clbp_center_descriptor_function = code_center

   end function clbp_center_descriptor_function

end module clbp_center_descriptor
