!-----------------------------------------------------------------------
! Module: Lower_ltp_module
! Author: Pedro Garcia Freitas
! Date: May 17, 2024.
! Description: This module provides basic operations to compute the
!              lower bownd of the local ternary pattern (LTP) descriptor 
!              More details: https://doi.org/10.1109/QoMEX48832.2020.9123076
!-----------------------------------------------------------------------
module lower_ltp_module
   implicit none

contains

   !&<
   subroutine iterator_lower_ltp(&
           &geometry, rows, cols, &
           &color, rows_color, cols_color, &
           &number_of_neighbors, &
           &map_labels)
      !&>
      use abstract_descriptor
      use lower_ltp_descriptor

      implicit none
      ! Inputs
      integer, intent(in) :: rows, cols
      real, dimension(rows, cols), intent(in) :: geometry
      integer, intent(in) :: rows_color, cols_color
      real, dimension(rows_color, cols_color), intent(in) :: color
      integer, intent(in) :: number_of_neighbors
      ! Outputs
      integer, dimension(rows_color, cols_color), intent(out) :: map_labels

      !&<
      call abstract_iterator(&
              &descriptor_function=lower_ltp_descriptor_function, &
              &geometry=geometry, rows=rows, cols=cols, &
              &color=color, rows_color=rows_color, cols_color=cols_color, &
              &number_of_neighbors=number_of_neighbors, &
              &map_labels=map_labels)
      !&>
   end subroutine iterator_lower_ltp

end module lower_ltp_module
