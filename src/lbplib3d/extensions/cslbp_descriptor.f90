!-----------------------------------------------------------------------
! Module: cslbp_descriptor
! Author: Maria Luiza Macedo Bezerra
! Date: August 14, 2024.
! Description: This code implements the local operations to compute
!              an center simetric local binary pattern (cslbp)inspired
!              descriptor using the distance vector of each point,
!              considering the center as reference.
!-----------------------------------------------------------------------
module cslbp_descriptor
   implicit none

contains

   real function cslbp_descriptor_function(target_point, neighbors)
      use common
      use kdtree
      implicit none
      ! Input/Interface function
      type(Point), intent(in) :: target_point
      type(Neighbor), allocatable, intent(in) :: neighbors(:)
      ! Specific variable functions
      integer :: n, i
      integer :: code
      real :: treshold, point_color_1, point_color_2, gray_dif
      real :: cos_value, min_cos, center
      integer, dimension(size(neighbors))  :: pairs

      center = target_point%color(1)
      code = 0
      treshold = 0.01
      min_cos = 1.0
      pairs(:) = 0
      point_color_1 = 0
      point_color_2 = 0
      gray_dif = 0
      cos_value = 0
      n = 0
      i = 0

      do n = 1, size(neighbors)
         do i = n+1, size(neighbors)
            cos_value = vector_cos_calculator(neighbors, target_point, n, i)
            if (cos_value < min_cos) then
               min_cos = cos_value
               pairs(n) = i
            end if
         end do
         min_cos = 0
      end do

      do n = 1, size(neighbors)/2
         point_color_1 = neighbors(n)%point_%color(1)
         point_color_2 = neighbors(pairs(n))%point_%color(1)
         gray_dif = abs(point_color_1 - point_color_2)
         code = IOR(code, IBSET(0, n - 1)* &
            &logical_to_integer(gray_dif > treshold))
      end do

      cslbp_descriptor_function = code

   end function cslbp_descriptor_function

   real function vector_cos_calculator(neighbors, target_point, n, i)
      use kdtree
      implicit none
      integer, intent(in) :: n, i
      type(Point), intent(in) :: target_point
      type(Neighbor), intent(in) :: neighbors(:)
      real :: point_coord_1, point_coord_2
      real :: abs_value_1, abs_value_2, internal_prod, cos_value
      integer :: j

      abs_value_1 = 0
      abs_value_2 = 0
      internal_prod = 0
      point_coord_1 = 0
      point_coord_2 = 0
      cos_value = 0
      j = 0
      do j = 1, 3
         point_coord_1 = neighbors(n)%point_%coord(j) - target_point%coord(j)
         point_coord_2 = neighbors(i)%point_%coord(j) - target_point%coord(j)
         internal_prod = internal_prod + (point_coord_1*point_coord_2)
         abs_value_1 = abs_value_1 + point_coord_1**2
         abs_value_2 = abs_value_2 + point_coord_2**2
      end do

      abs_value_1 = sqrt(abs_value_1)
      abs_value_2 = sqrt(abs_value_2)
      cos_value = internal_prod/(abs_value_1*abs_value_2)

      vector_cos_calculator = cos_value
   end function vector_cos_calculator

end module cslbp_descriptor
