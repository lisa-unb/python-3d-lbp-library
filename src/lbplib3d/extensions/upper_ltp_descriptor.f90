!-----------------------------------------------------------------------
! Module: upper_ltp_descriptor
! Author: Maria Luiza MAcedo Bezerra
! Date: May 20, 2024.
! Description: This code implements the local operations to compute the
!              upper bound of the 3D Local Ternary Pattern (LTP) descriptor
!              using a target (point) and their nearest neighbors.
!-----------------------------------------------------------------------
module upper_ltp_descriptor
   implicit none

contains

   real function upper_ltp_descriptor_function(target_point, neighbors)
      use common
      use kdtree
      implicit none
      ! Input/Interface function
      type(Point), intent(in) :: target_point
      type(Neighbor), allocatable, intent(in) :: neighbors(:)
      ! Specific variable functions
      integer :: n, treshold, difference
      integer :: code
      real :: center, point_color

      center = target_point%color(1)*255.0
      code = 0
      treshold = 6
      do n = 1, size(neighbors)
         point_color = 255*neighbors(n)%point_%color(1)
         difference = point_color - center

         if (difference >= treshold .or. difference <= -treshold) then
            code = IOR(code, IBSET(0, n - 1))
         else if (abs(difference) <= treshold) then
            code = ior(code, IBSET(0, n - 1)*0)
         end if
      end do
      upper_ltp_descriptor_function = code
   end function upper_ltp_descriptor_function

end module upper_ltp_descriptor
