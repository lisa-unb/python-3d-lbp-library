"""
Module: olbp.py
Author: Pedro Garcia Freitas
Date: May 17, 2024
Description: This module provides functions for computing the Center-symmetric
local binary pattern (CSLBP) feature maps.
"""

import numpy as np
import numpy.typing as npt
import open3d as o3d
from lbplib3d.extensions.cslbp_module import cslbp_module
from lbplib3d._helpers import compute_feature_histogram
from lbplib3d._helpers import abstract_lbp_iterator_feature_extract


def cslbp(
    pcd: o3d.geometry.PointCloud, neighbors: int = 8
) -> o3d.geometry.PointCloud:
    """
    Computes the Center-Symmetric Local Binary Pattern (OLBP) feature map
    of an input color Point Cloud.

    Parameters:
        pcd (o3d.geometry.PointCloud): Input point cloud from open3d.
        neighbors (int): Number of LBP neighbors

    Returns:
        o3d.geometry.PointCloud: 3D feature map computed using OLBP algorithm.
    """
    return abstract_lbp_iterator_feature_extract(
        pcd, neighbors, cslbp_module.iterator_cslbp
    )


def cslbp_feature(
    pcd: o3d.geometry.PointCloud, neighbors: int = 8
) -> npt.NDArray[np.int_]:
    """
    Computes the histogram of Center-Symmetric Local Binary Pattern (CSLBP)
    descriptor from the feature map of an input color Point Cloud.

    Parameters:
        pcd (o3d.geometry.PointCloud): Input point cloud from open3d.
        neighbors (int): Number of LBP neighbors

    Returns:
        o3d.geometry.PointCloud: Histogram features of CSLBP feature map.
    """
    feature_map = cslbp(pcd, neighbors)
    number_bins = 2 ** (neighbors // 2)
    return compute_feature_histogram(feature_map, number_bins=number_bins)
