# Create the content for CONTRIBUTING.md
contributing_content = """
# Contributing to lbplib3d

Thank you for your interest in contributing to lbplib3d! This document provides
guidelines to help you get started.

## Table of Contents

1. [Project Overview](#project-overview)
2. [Project Structure](#project-structure)
3. [How to Contribute](#how-to-contribute)
    - [Reporting Issues](#reporting-issues)
    - [Submitting Pull Requests](#submitting-pull-requests)
4. [Development Setup](#development-setup)
    - [Prerequisites](#prerequisites)
    - [Installation](#installation)
5. [Code Guidelines](#code-guidelines)
6. [Testing](#testing)
7. [Contact](#contact)

## Project Overview

lbplib3d is a library designed to compute 3D texture descriptors, such as Local
 Binary Patterns (LBP) and its variants. This library provides efficient
algorithms and tools for 3D texture analysis, which can be used in various 
applications including computer vision, medical imaging, and 3D shape analysis.

## Project Structure

The project follows a structured layout to keep the code organized and
maintainable:


- **src/lbplib3d/**: Contains the main Python module for the lbplib3d library.
- **src/lbplib3d/extensions/**: Contains Fortran extensions.
- **tests/**: Contains test cases for the library.

## How to Contribute

### Reporting Issues

If you encounter any issues while using lbplib3d, please report them by
creating an issue in the GitHub repository. Make sure to include details about
the problem and steps to reproduce it.

### Submitting Pull Requests

1. Fork the repository on GitHub.
2. Create a new branch from `main` for your feature or bug fix.
3. Implement your changes.
4. Ensure your changes follow the [Code Guidelines](#code-guidelines) and pass all tests.
5. Commit your changes and push your branch to GitHub.
6. Open a pull request to the `main` branch of the original repository.

Please provide a detailed description of your changes in the pull request.

## Development Setup

### Prerequisites

- Python 3.7+
- Fortran compiler (e.g., `gfortran`)
- `f2py` (part of NumPy)

### Building

1. Clone the repository:

    ```sh
    git clone https://gitlab.com/lisa-unb/python-3d-lbp-library.git
    cd python-3d-lbp-library
    ```

2. Install the required Python packages:

    ```sh
    micromamba create -f environment.yml
    ```

3. Compile the Fortran extensions:

    ```sh
    cd src
    python setup.py build_ext --inplace
    ```

## Code Guidelines

- Follow PEP 8 for Python code.
- For Fortran code, ensure proper indentation and comments for readability.
- Write meaningful commit messages.
- Include docstrings for all functions and classes.

## Testing

To run the tests, use the following command:

```sh
pytest
```

## Files Organization in `src/lbplib3d/extensions`

In the `src/lbplib3d/extensions` directory, files are organized following a specific format:

### Descriptor Files

Each descriptor is implemented in two files:

- `<descriptor name>_descriptor.f90`: This file contains the core descriptor function that is injected into `abstract_descriptor.f90`.
- `<descriptor name>_module.f90`: This file serves as an interface to expose the Fortran code to Python using `f2py`.

#### Example

For instance, for the Original LBP descriptor:

- **Files**:
  - `olbp_descriptor.f90`
  - `olbp_module.f90`

- **Purpose**:
  - `olbp_descriptor.f90`: Implements the core descriptor function that is injected into `abstract_descriptor.f90`.
  - `olbp_module.f90`: Acts as an interface to expose the Fortran code to Python using `f2py`.

This organization ensures that each descriptor's functionality and interface are clearly separated and properly exposed for Python integration.

### Automatic Fortran Files Formatting

We recommend to use [fprettify](https://github.com/fortran-lang/fprettify) to format the fortran files. For instance:

```sh
fprettify lbplib3d/extensions/common.f90 
```


### Automatic C++ Files Formatting

- Follow the Mozilla coding style guidelines for C++ code.
- Write clear and concise comments to explain your code.
- Use meaningful variable and function names.
- Adhere to the project's build and testing instructions.
- We recommed to format c++ files using _clang-format_: `clang-format --style=Mozilla -i your_file.hpp`
