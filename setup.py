import glob
import os
import subprocess

import numpy
from setuptools import setup, find_packages
from setuptools.command.build_ext import build_ext
from setuptools.command.install import install


base_compile_args = ["-std=f2008", "-fopenmp", "-fPIC", "-O3"]
# base_compile_args += ["-g", "-fbacktrace", "-fcheck=all"] # used for debug

build_dir = "src/lbplib3d/extensions/"


class CustomBuildExt(build_ext):
    def run(self):
        self.common_cpp_sources = ["knn_wrapper"]
        self.common_fortran_sources = [
            "common",
            "knn_interface",
            "kdtree",
            "abstract_descriptor",
            "olbp_descriptor",
            "ulbp_descriptor",
            "nriulbp_descriptor",
            "rorlbp_descriptor",
            "upper_ltp_descriptor",
            "lower_ltp_descriptor",
            "dif_ltp_descriptor",
            "rltp_descriptor",
            "clbp_magnitude_descriptor",
            "clbp_signs_descriptor",
            "clbp_center_descriptor",
            "lower_cltp_magnitude_descriptor",
            "upper_cltp_magnitude_descriptor",
            "lower_cltp_center_descriptor",
            "upper_cltp_center_descriptor",
            "lower_cltp_signs_descriptor",
            "upper_cltp_signs_descriptor",
            "cslbp_descriptor",
        ]

        self.extension_fortran_sources = [
            "olbp_module",
            "ulbp_module",
            "nriulbp_module",
            "rorlbp_module",
            "upper_ltp_module",
            "lower_ltp_module",
            "dif_ltp_module",
            "rltp_module",
            "clbp_magnitude_module",
            "clbp_signs_module",
            "clbp_center_module",
            "upper_cltp_center_module",
            "lower_cltp_center_module",
            "upper_cltp_signs_module",
            "lower_cltp_signs_module",
            "upper_cltp_magnitude_module",
            "lower_cltp_magnitude_module",
            "cslbp_module",
        ]

        # Compile Fortran sources separately
        self.compile_fortran_sources()
        # Run the original build_ext command
        super().run()

    @staticmethod
    def compiler(compiler, source):
        full_path_src = os.path.join(build_dir, source)
        cmd = [compiler, "-c", full_path_src] + base_compile_args
        print("Running:", " ".join(cmd))
        subprocess.check_call(cmd)

    def compile_fortran(self, source):
        CustomBuildExt.compiler("gfortran", f"{source}.f90")

    def compile_cpp(self, source):
        CustomBuildExt.compiler("gcc", f"{source}.cpp")

    def compile_common_modules(self):
        for source in self.common_cpp_sources:
            self.compile_cpp(source)

        for source in self.common_fortran_sources:
            self.compile_fortran(source)

    def compile_extensions_modules(self):
        for source in self.extension_fortran_sources:
            self.compile_fortran(source)

    def build_and_link_with_f2py(self):
        # Compile the final module with f2py separately from modules
        for module in self.extension_fortran_sources:
            module_path = os.path.join(build_dir, f"{module}.f90")
            builded = self.common_fortran_sources + self.common_cpp_sources
            common_dependencies = [f"{m}.o" for m in builded]
            f2py_cmd = ["f2py", "-c", module_path]
            f2py_cmd += common_dependencies
            f2py_cmd += [
                "--fcompiler=gfortran",
                "--f90flags='-fopenmp'",
                "-lgomp",
                "-lstdc++",
                "-m",
                module,
            ]
            print("Running:", " ".join(f2py_cmd))
            subprocess.check_call(f2py_cmd)

    def clean(self):
        for file in glob.glob(r"*.so"):
            dst = os.path.join(build_dir, file)
            print(f"Moving: {file} -> {dst}")
            os.rename(file, dst)

        for file in glob.glob(r"*.o") + glob.glob(r"*.mod"):
            print("Removing: ", file)
            os.remove(file)

    def compile_fortran_sources(self):
        self.compile_common_modules()
        self.compile_extensions_modules()
        self.build_and_link_with_f2py()
        self.clean()


class CustomInstall(install):
    def run(self):
        self.run_command("build_ext")
        install.run(self)


def get_version(path: str) -> str:
    here = os.path.abspath(os.path.dirname(__file__))
    version_file = os.path.join(here, path)
    with open(version_file, "r") as fp:
        for line in fp:
            if line.startswith("__version__"):
                delimiter = '"' if '"' in line else "'"
                return line.split(delimiter)[1]
    raise RuntimeError(f"Unable to find the version string in {version_file}.")


if __name__ == "__main__":
    THIS_PACKAGE_NAME = "lbplib3d"
    VERSION = get_version(
        os.path.join("src", THIS_PACKAGE_NAME, "__init__.py")
    )

    setup(
        name=THIS_PACKAGE_NAME,
        version=VERSION,
        description="A Python library for 3D LBP using Fortran extensions",
        cmdclass={
            "build_ext": CustomBuildExt,
            "install": CustomInstall,
        },
        include_dirs=[numpy.get_include()],
        packages=find_packages(where='src'),
        package_dir={'': 'src'},
        data_files=[
            ("lbplib3d/extensions", glob.glob(os.path.join(build_dir, "*.so")))
        ],
        package_data={
            "lbplib3d": ["extensions/*.so"],  # Ensure .so files are included
        },
        include_package_data=True,
        zip_safe=False,
    )
