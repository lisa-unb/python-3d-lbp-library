# Python 3D LBP Library

## Requirements

Run `micromamba create -f environment.yml` to install the required packages.

## Instalation

1. Clone the repository:

    ```sh
    git clone https://gitlab.com/lisa-unb/python-3d-lbp-library.git
    cd python-3d-lbp-library
    ```

2. Install the required Python packages:

    ```sh
    micromamba create -f environment.yml
    ```

3. Compile the Fortran extensions:

    ```sh
    cd src
    python setup.py build
    python setup.py install
    ```
