import sys

sys.path.append("../src")

try:
    import numpy as np
    import open3d as o3d
    import utils
    from lbplib3d.olbp import olbp
    from lbplib3d.ulbp import ulbp
    from lbplib3d.nriulbp import nriulbp
    from lbplib3d.rorlbp import rorlbp
    from lbplib3d.ltp import upper_ltp
    from lbplib3d.ltp import lower_ltp
    from lbplib3d.ltp import rltp
    from lbplib3d.ltp import dif_ltp
    from lbplib3d.clbp import clbp_magnitude
    from lbplib3d.clbp import clbp_signs
    from lbplib3d.clbp import clbp_center
    from lbplib3d.cltp import upper_cltp_magnitude
    from lbplib3d.cltp import upper_cltp_signs
    from lbplib3d.cltp import upper_cltp_center
    from lbplib3d.cltp import lower_cltp_magnitude
    from lbplib3d.cltp import lower_cltp_signs
    from lbplib3d.cltp import lower_cltp_center
    from lbplib3d.cslbp import cslbp
    from lbplib3d._helpers import compute_feature_histogram
except Exception:
    raise

pc_filepath = "longdress_vox10_1300_vox10_octree08_jpeg010.ply"
pc = utils.safe_read_point_cloud(pc_filepath)


def abstract_feature_maps_test(descriptor_function):
    test_name = descriptor_function.__name__
    print(f"Running test_{test_name}")
    feature_map = descriptor_function(pc)
    assert np.array_equal(pc.points, feature_map.points)
    o3d.io.write_point_cloud(f"{test_name}.ply", feature_map, write_ascii=True)


def test_olbp():
    abstract_feature_maps_test(olbp)


def test_ulbp():
    abstract_feature_maps_test(ulbp)


def test_nriulbp():
    abstract_feature_maps_test(nriulbp)


def test_rorlbp():
    abstract_feature_maps_test(rorlbp)


def test_upper_ltp():
    abstract_feature_maps_test(upper_ltp)


def test_lower_ltp():
    abstract_feature_maps_test(lower_ltp)


def test_rltp():
    abstract_feature_maps_test(rltp)


def test_dif_ltp():
    abstract_feature_maps_test(dif_ltp)


def test_clbp_magnitude():
    abstract_feature_maps_test(clbp_magnitude)


def test_clbp_signs():
    abstract_feature_maps_test(clbp_signs)


def test_clbp_center():
    abstract_feature_maps_test(clbp_center)


def test_lower_cltp_magnitude():
    abstract_feature_maps_test(lower_cltp_magnitude)


def test_lower_cltp_signs():
    abstract_feature_maps_test(lower_cltp_signs)


def test_lower_cltp_center():
    abstract_feature_maps_test(lower_cltp_center)


def test_upper_cltp_magnitude():
    abstract_feature_maps_test(upper_cltp_magnitude)


def test_upper_cltp_signs():
    abstract_feature_maps_test(upper_cltp_signs)


def test_upper_cltp_center():
    abstract_feature_maps_test(upper_cltp_center)


def test_cslbp():
    abstract_feature_maps_test(cslbp)


def test_feature_maps():
    test_cslbp()
    test_lower_cltp_magnitude()
    test_lower_cltp_signs()
    test_lower_cltp_center()
    test_upper_cltp_center()
    test_upper_cltp_magnitude()
    test_upper_cltp_signs()
    test_clbp_center()
    test_clbp_magnitude()
    test_clbp_signs()
    test_dif_ltp()
    test_upper_ltp()
    test_lower_ltp()
    test_rltp()
    test_olbp()
    test_rorlbp()
    test_ulbp()


if __name__ == "__main__":
    test_feature_maps()
