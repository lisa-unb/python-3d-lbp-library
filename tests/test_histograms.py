import sys
import unittest
import open3d as o3d

sys.path.append("../src")

try:
    import utils
    from lbplib3d.olbp import olbp_feature
    from lbplib3d.ulbp import ulbp_feature
    from lbplib3d.nriulbp import nriulbp_feature
    from lbplib3d.rorlbp import rorlbp_feature
    from lbplib3d.ltp import ltp_feature
    from lbplib3d.clbp import clbp_feature
    from lbplib3d.cltp import cltp_feature
    from lbplib3d.cslbp import cslbp_feature
except Exception:
    raise


class TestHistograms(unittest.TestCase):
    def __init__(self, descriptor_function, expected_bins):
        super(TestHistograms, self).__init__("test_histogram")
        self.expected_bins = expected_bins
        self.descriptor_function = descriptor_function

    def setUp(self):
        pc_filepath = "longdress_vox10_1300_vox10_octree08_jpeg010.ply"
        self.pc = utils.safe_read_point_cloud(pc_filepath)

    def test_histogram(self):
        histogram = self.descriptor_function(self.pc)
        print(">> Histograms: ", histogram)
        self.assertEqual(len(histogram), self.expected_bins)


def abstract_histogram_test(description_function, expected_bins):
    test_name = description_function.__name__
    print(f"Running test_{test_name}")
    test_suite = unittest.TestSuite()
    test_suite.addTest(TestHistograms(description_function, expected_bins))
    runner = unittest.TextTestRunner()
    runner.run(test_suite)


def test_olbp():
    neighbors = 8

    def olbp_histogram(pc: o3d.geometry.PointCloud):
        return olbp_feature(pc, neighbors)

    abstract_histogram_test(olbp_histogram, 2**neighbors)


def test_ulbp():
    neighbors = 8

    def ulbp_histogram(pc: o3d.geometry.PointCloud):
        return ulbp_feature(pc, neighbors)

    abstract_histogram_test(ulbp_histogram, neighbors + 2)


def test_nriulbp():
    neighbors = 8

    def nriulbp_histogram(pc: o3d.geometry.PointCloud):
        return nriulbp_feature(pc, neighbors)

    expected_bins = neighbors * (neighbors + 1) + 2
    abstract_histogram_test(nriulbp_histogram, expected_bins)


def test_rorlbp():
    neighbors = 8

    def rorlbp_histogram(pc: o3d.geometry.PointCloud):
        return rorlbp_feature(pc, neighbors)

    abstract_histogram_test(rorlbp_histogram, 2**neighbors)


def test_ltp():
    neighbors = 8

    def ltp_histogram(pc: o3d.geometry.PointCloud):
        return ltp_feature(pc, neighbors)

    abstract_histogram_test(ltp_histogram, 4 * (2**neighbors))


def test_clbp():
    neighbors = 8

    def clbp_histogram(pc: o3d.geometry.PointCloud):
        return clbp_feature(pc, neighbors)

    histogram_size = 2 + 2**neighbors + 2**neighbors
    abstract_histogram_test(clbp_histogram, histogram_size)


def test_cltp():
    neighbors = 8

    def cltp_histogram(pc: o3d.geometry.PointCloud):
        return cltp_feature(pc, neighbors)

    histogram_size = 2 * (2 + 2**neighbors + 2**neighbors)
    abstract_histogram_test(cltp_histogram, histogram_size)


def test_cslbp():
    neighbors = 8

    def cslbp_histogram(pc: o3d.geometry.PointCloud):
        return cslbp_feature(pc, neighbors)

    number_bins = 2 ** (neighbors // 2)
    abstract_histogram_test(cslbp_histogram, number_bins)


def test_histograms():
    test_cslbp()
    test_cltp()
    test_clbp()
    test_ltp()
    test_olbp()
    test_nriulbp()
    test_rorlbp()
    test_ulbp()


if __name__ == "__main__":
    test_histograms()
