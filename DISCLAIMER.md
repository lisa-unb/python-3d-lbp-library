# DISCLAIMER

This software uses the following Open Source component(s):

* **"nanoflann"** version 1.6.1, available at [https://github.com/jlblancoc/nanoflann](https://github.com/jlblancoc/nanoflann) and distributed under a **BSD License**. Copyright (c) 2008-2009  Marius Muja (mariusm@cs.ubc.ca). Copyright 2008-2009  David G. Lowe (lowe@cs.ubc.ca). Copyright 2011 Jose L. Blanco (joseluisblancoc@gmail.com). All rights reserved.

